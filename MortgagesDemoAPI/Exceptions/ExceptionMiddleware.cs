﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Datadog.Trace;
using System.Text.Json;

namespace MortgagesDemoAPI.Exceptions
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate next;
        public ExceptionMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context) {
            try {
                await next(context);
            }
            catch(Exception ex) {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception) {

            // Default Error Code
            var code = HttpStatusCode.InternalServerError;

            if (exception is KeyNotFoundException) code = HttpStatusCode.NotFound;
            if (exception is TooYoungException) code = HttpStatusCode.Forbidden;
            if (exception is MissingFieldException) code = HttpStatusCode.BadRequest;
            
            var result = JsonSerializer.Serialize(new { error = exception.Message });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}
