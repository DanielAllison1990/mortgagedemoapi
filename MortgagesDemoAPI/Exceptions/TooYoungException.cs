﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MortgagesDemoAPI.Exceptions
{
    public class TooYoungException : Exception
    {
        public TooYoungException() { }

        public TooYoungException(string message) : base(message) { }

        public TooYoungException(string message, Exception ex) : base(message, ex) { }
    }
}
