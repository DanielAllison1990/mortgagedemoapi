using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using MortgagesDemoAPI.Data.Context;
using MortgagesDemoAPI.Domain.Entities;
using MortgagesDemoAPI.Exceptions;
using MortgagesDemoAPI.Repositories;
using MortgagesDemoAPI.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MortgagesDemoAPI
{
    public class Startup
    {
        readonly string AllowedOrigins = "_allowedOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MortgageDemoContext>(options =>
              options.UseSqlServer(Configuration.GetConnectionString("MortgageDemoContext")));
            
            services.AddControllers()
                .AddJsonOptions(options => {
                    options.JsonSerializerOptions.PropertyNamingPolicy = null; // Stops camel casing json return
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()); // Sets swagger enum to display as string
                });


            services.AddCors(options => 
                {
                    options.AddPolicy(AllowedOrigins,

                        builder =>
                        {
                            // Not normally what would be done, but much easier for sharing 
                            builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                        });
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "MortgagesDemoAPI", Version = "v1" });
            });

            services.AddMvc().AddFluentValidation();

            ImplementDIService(services);
        }

        private void ImplementDIService(IServiceCollection services) {
            services.AddScoped<MortgageDemoContext>();
            services.AddTransient<IValidator<User>, UserValidator>();
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MortgagesDemoAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseCors(AllowedOrigins);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
