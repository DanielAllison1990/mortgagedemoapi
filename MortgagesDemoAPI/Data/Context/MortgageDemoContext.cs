﻿using Microsoft.EntityFrameworkCore;
using MortgagesDemoAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MortgagesDemoAPI.Domain.Enums;

namespace MortgagesDemoAPI.Data.Context
{
    public class MortgageDemoContext : DbContext
    {

        public MortgageDemoContext(DbContextOptions<MortgageDemoContext> options) : base(options) { 
        
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<MortgageProvider>().ToTable("MortgageProviders");
        }
        public DbSet<User> Users { get; set; }
        public DbSet<MortgageProvider> MortgageProviders { get; set; }

    }

}
