﻿using Microsoft.EntityFrameworkCore;
using MortgagesDemoAPI.Data.Context;
using MortgagesDemoAPI.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MortgagesDemoAPI.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {

        internal MortgageDemoContext _context;
        internal DbSet<T> _dbSet;

        public BaseRepository(MortgageDemoContext context) {
            _context = context;
            _dbSet = context.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public IEnumerable<T> Get(Expression<Func<T, bool>> where, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, List<string> includes = null)
        {
            IQueryable<T> query = _dbSet;

            query = query.Where(where);

            if(includes != null)
            {
                foreach (var include in includes)
                {
                    query = query.Include(include);
                }
            }

            if(orderBy != null)
            {
                return orderBy(query).ToList();
            }

            return query.ToList();
        }

        public T GetById(object id)
        {
           return _dbSet.Find(id);
        }

        public int Count()
        {
            return _dbSet.Count();
        }

        public int Max(Func<T, int> key)
        {
            return _dbSet.Max(key);
        }

        public T Insert(T entity)
        {
            return _dbSet.Add(entity).Entity;
        }
        

        public void Update(T entity)
        {
            _dbSet.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> items = _dbSet.Where(where).AsEnumerable();

            if (items.Count() == 0) {
                throw new KeyNotFoundException("No Items to delete");
            }

            foreach (var item in items)
            {
                _context.Entry(item).State = EntityState.Deleted;
            }

            _dbSet.RemoveRange(items);
        }

        public void Delete(T entity) {
            if (_context.Entry(entity).State == EntityState.Detached) {
                _dbSet.Attach(entity);
            }

            _dbSet.Remove(entity);
        }

        public void Delete(object id)
        {
            T entityToDelete = _dbSet.Find(id);

            if (entityToDelete == null) {
                throw new KeyNotFoundException("Cannot Find Entity");
            }

            Delete(entityToDelete);

        }


        public int SaveChanges()
        {
            return _context.SaveChanges();
        }


    }
}
