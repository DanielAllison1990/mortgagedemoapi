﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MortgagesDemoAPI.Repositories.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        IEnumerable<T> Get(
            Expression<Func<T, bool>> where, Func<IQueryable<T>,
            IOrderedQueryable<T>> orderBy = null,
            List<string> includes = null);

        // Object in case of guid Id
        T GetById(object id);

        IEnumerable<T> GetAll();

        int Count();

        int Max(Func<T, int> key);

        T Insert(T entity);

        void Update(T entity);

        void Delete(Expression<Func<T, bool>> where);

        void Delete(object id);
        void Delete(T entity);

        int SaveChanges();

    }
}
