﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MortgagesDemoAPI.Domain.Enums
{
    public enum MortgageType
    {
        Variable = 1,
        Fixed = 2
    }
}
