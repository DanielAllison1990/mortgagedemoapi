﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MortgagesDemoAPI.Data.Context;
using MortgagesDemoAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MortgagesDemoAPI.Domain
{
    public static class SeedData
    {
        // Make sure to run this first otherwise you get an error about permissions
        // Run it in PackageManagerConsole
        // Add-Migration InitialCreate
        // Update-Database
        public static void Initialize(IServiceProvider serviceProvider)
        {

            using (var context = new MortgageDemoContext(serviceProvider.GetRequiredService<DbContextOptions<MortgageDemoContext>>()))
            {
                if (context.MortgageProviders.Any())
                {
                    return;
                }
                else
                {
                    context.AddRange(new MortgageProvider()
                    {
                        InterestRate = 2,
                        Lender = "Bank A",
                        LoanToValue = 60,
                        Type = Enums.MortgageType.Variable
                    },

                    new MortgageProvider()
                    {
                        InterestRate = 3,
                        Lender = "Bank B",
                        LoanToValue = 60,
                        Type = Enums.MortgageType.Fixed
                    },

                    new MortgageProvider()
                    {
                        InterestRate = 4,
                        Lender = "Bank C",
                        LoanToValue = 90,
                        Type = Enums.MortgageType.Variable
                    });
                }

                if (context.Users.Any())
                {
                    return;
                }
                else
                {
                    context.AddRange(new User()
                    {
                        Email = "dantest@gmail.com",
                        FirstName = "Daniel",
                        LastName = "Allison",
                        DateOfBirth = new DateTime(1950, 10, 10)
                    });

                }

                context.SaveChanges();

            }
        }
    }
}
