﻿using FluentValidation;
using MortgagesDemoAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MortgagesDemoAPI.Domain.Entities
{
    [Table("User")]
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [NotMapped]
        public string FullName
        {
            get
            {
                return $"{this.FirstName} {this.LastName}";
            }
        }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
    }
}

// Fluent Validation to add nice rules to the validator
// Also means we don't need to check for model state validity in each controller
public class UserValidator : AbstractValidator<User>
{
    public UserValidator()
    {
        RuleFor(x => x.Email).EmailAddress();
        //RuleFor(x => x.DateOfBirth).LessThanOrEqualTo(DateTime.Now.AddYears(-18));
    }
}
