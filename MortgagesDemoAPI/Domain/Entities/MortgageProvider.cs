﻿using MortgagesDemoAPI.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MortgagesDemoAPI.Domain.Entities
{
    [Table("MortgageProviders")]
    public class MortgageProvider
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Lender { get; set; }
        public decimal InterestRate { get; set; }
        public MortgageType Type { get; set; }

        public decimal LoanToValue { get; set; }

    }
}