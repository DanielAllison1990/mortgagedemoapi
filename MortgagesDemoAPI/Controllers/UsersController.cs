﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MortgagesDemoAPI.Domain.Entities;
using MortgagesDemoAPI.Repositories.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MortgagesDemoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        public readonly IBaseRepository<User> _repository;

        public UsersController(IBaseRepository<User> repository) {
            this._repository = repository;
        }

        [HttpGet]
        public IActionResult GetUsers() {
            var allUsers = this._repository.GetAll();
            return Ok(allUsers);
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetUser([Required]int id)
        {
            var user = this._repository.GetById(id);
            return Ok(user);
        }

        // Accept user details from brief
        // Adds the user to a database
        // Returns the unique id for that user
        [HttpPost]
        public IActionResult CreateUser([FromBody] User user) {
            _repository.Insert(user);
            _repository.SaveChanges();

            return Ok(user.Id);
        }

        [HttpPut]
        public IActionResult UpdateUser([FromBody] User user)
        {
            var dbUser = _repository.GetById(user.Id);

            if (dbUser == null) {
                throw new KeyNotFoundException("No User found to update");
            }

            dbUser.FirstName = user.FirstName != dbUser.FirstName ? user.FirstName : dbUser.FirstName;
            dbUser.LastName = user.LastName != dbUser.LastName ? user.LastName : dbUser.LastName;
            dbUser.Email = user.Email != dbUser.Email ? user.Email : dbUser.Email;
            dbUser.DateOfBirth = user.DateOfBirth != dbUser.DateOfBirth ? user.DateOfBirth : dbUser.DateOfBirth;

            _repository.Update(dbUser);

            _repository.SaveChanges();

            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteUser([FromBody] User user)
        {
            _repository.Delete(user);
            _repository.SaveChanges();

            return Ok();
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteUser([Required]int id)
        {
            _repository.Delete(id);
            _repository.SaveChanges();

            return Ok();
        }
    }
}
