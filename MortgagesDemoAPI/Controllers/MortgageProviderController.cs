﻿using Microsoft.AspNetCore.Mvc;
using MortgagesDemoAPI.Domain.Entities;
using MortgagesDemoAPI.Exceptions;
using MortgagesDemoAPI.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MortgagesDemoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MortgageProviderController : ControllerBase
    {
        public readonly IBaseRepository<MortgageProvider> _mortgageRepository;
        public readonly IBaseRepository<User> _userRepostiry;

        public MortgageProviderController(IBaseRepository<MortgageProvider> mortgageRepository, IBaseRepository<User> userRepsository)
        {
            this._mortgageRepository = mortgageRepository;
            this._userRepostiry = userRepsository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var allMortgageProviders = this._mortgageRepository.GetAll();
            return Ok(allMortgageProviders);
        }

        // This is the main method for the brief for mortgage returns
        // Gets Mortgages if user is over 18
        // And if the LTV is <90%
        [HttpGet]
        [Route("GetProviderBasedOnLTV")]
        public IActionResult GetProviderBasedOnLTV([Required]decimal propertyValue, [Required]decimal depositAmount, [Required]int userId)
        {

            if (depositAmount >= propertyValue)
            {
                throw new KeyNotFoundException("You do not need a mortgage, you have enough to pay for the house");
            }

            if (propertyValue == 0) {
                throw new MissingFieldException("Please provide a property value");
            }

            var user = this._userRepostiry.Get(xx => xx.Id == userId).FirstOrDefault();

            if (user == null) {
                throw new KeyNotFoundException("User does not exist");
            }

            if (DateTime.Now.Year - user.DateOfBirth.Year < 18) {
                throw new TooYoungException("You are too young to get a mortgage");
            }

            var amountBorrowed = propertyValue - depositAmount;
            var ltv = Math.Round((amountBorrowed / propertyValue) * 100, 2);

            if (ltv > 90) {
                throw new KeyNotFoundException("No Providers offer mortgages for an ltv of greater than 90%");
            }

            var mortgageProviders = this._mortgageRepository.Get(xx => ltv < xx.LoanToValue).ToList();

            return Ok(mortgageProviders);
        }
    }
}
