# README #

### What is this repository for? ###

* 1

### How do I get set up? ###

* Make sure to run this first otherwise you get an error about permissions
* It is to Create the initial migration to add the database to local machine
* I ran it on visual studio community 2019 using MSSQLLocalDb
* Run these commands in PackageManagerConsole
* Add-Migration InitialCreate
* Update-Database

* After the migration is done you can check the status of the database in the sql server object explorer
* Expand the MSSQLLocalDb > Expand Databases > Should see MortgageDemoContext

* There should be one database and two tables inside it
* One for the mortgage providers and one for the users
* Both will be seeded with demo data, the providers with the detaisl from the spec
* And the users with a test user

* There are extra method calls to test adding / editing and delting of users

* In the run dropdown in the toolbar select the MortgagesDemoAPI not the IISExpress
* this runs on port 5001 which the front end will be expecting

* After that it is all setup and ready to go

### How To Test API Independently ###

* Swagger API testing is setup on the API
* This means the calls in the controllers are available in the browser
* When you run the solution it will open up swagger in the browser
* Just expand the controller methods that you want to test
* CLick try it out and then enter the appropriate information


